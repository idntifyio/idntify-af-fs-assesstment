![Idntify Logo](https://cdn.idntify.io/idntify-logo.png)

# Full Stack Developer Test

Idntify has recently launched a really good solution that will stop fraud in Mexico.
This solution is based on data collected from any ecommerce users (the ecommerce will implement the solution) as well as some really nice graphs based on this data.
However, collecting the data is not enough to have this product up and running.
Idntify needs to understand this data and then see what else can be done with it.
Your task is to implement a dashboard and a server to make this product work.

---
## Dashboard

The dashoard should:

- Be a nice looking dashboard where Idntify can view the collected data.
- There are no more guidelines

---
## API

The API should:

- There are actually no guidelines for how you will construct your API. You have to register as many methods as you need to make it work.

---

#### Notes
- You have to gather information from the end users, what this assestment is about is to see really how much data you can gather from the end user. There are no guidelines really, and the more data you gather, the better.
- Your solution **MUST** be easy to implement on **ANY** ecommerce
- Needless to say, you must build the data schema. It must be clear, easy to understand and have a good structure.
- Also, needless to say. This requires you to **IMPLEMENT** something (your choice) on the end client. That means that ecommerce sites must either, implement your API or something else you want to make it work.
- The easier the integration for the ecommerce, the better.

---

#### The whole solution should:
- Have POSTGRES as the database system
- Be portable, can be built and executed in any system
- Have a clear structure (for both, the API and the Client)
- Be written in React for the frontend, and NodeJS + Express / Hapi for the backend
- Be easy to grow with new functionality 
- Don't include binaries, .sh files, etc. Use a dependency management tool (npm or yarn).

---
#### Bonus Points For:
- Tests, the more coverage the better
- Comments, a good readme, instructions, etc.
- Docker images / CI
- Commit messages (include .git in zip)
- Clear scalability

---
### You deliverable should have
- An API
- A client
- Instructions on how to do the integration from the client side
- A diagram of the whole architecture. Your architecture MUST be scalable and be able to handle millions of requests
- A diagram/schema/ERD of your database schema. Why it is modeled like that.
